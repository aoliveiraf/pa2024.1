
class Produto {
    constructor(doc) {
        this.doc = doc;
        this.product_key = doc.product_key;
        this.estoque_key = doc.estoque_key;
        this.quantidade = doc.quantidade;
    }
    json() {
        return this.doc;
    }
}


module.exports = Produto;
