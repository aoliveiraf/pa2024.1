package singleton;
import java.util.ArrayList;
import java.util.List;

import strategy.TextFormat;

public class Sistema {

    private static Sistema sistema;

    private List<TextFormat> textFormats = new ArrayList<>(); 

    List<Book> listBook = new ArrayList<Book>();
    
    private Sistema() {

    }

    public void addTextFormat(TextFormat tf) {

        this.textFormats.add(tf);
    }

    public String runTextFormat(int i,String text) {
        
        return this.textFormats.get(i).format(text);
    }
    
    public static Sistema instancia() {

        if(sistema == null) {
            sistema = new Sistema();
        }

        return sistema;

    }

    public void addLivro(Book b) {

        this.listBook.add(b);
    }

    public static void main(String [] args) {

        Sistema s = new Sistema();

        Sistema s1 = new Sistema();

    }
}