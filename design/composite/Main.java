package composite;

public class Main {
    public static void main(String[] args) {

        ComponenteComposite placaMae = new ComponenteComposite("placa Mae");  

        ComponenteComposite placaVideo = new ComponenteComposite("Placa video");

        ComponenteFolha barramento1 = new ComponenteFolha("Barramento 1");

        ComponenteFolha barramento2 = new ComponenteFolha("Barramento 2");

        ComponenteFolha processador =  new ComponenteFolha("Processador");

        ComponenteFolha circuitoIntegrado1 = new new ComponenteFolha("Processador");

        placaMae.addComponente(barramento1);

        placaMae.addComponente(circuitoIntegrado1);

        placaMae.addComponente(placaVideo);
        placaMae.addComponente(processador);

        placaVideo.addComponente(barramento2);

        
    }
}
