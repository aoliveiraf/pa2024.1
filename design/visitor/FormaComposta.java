package visitor;

import java.util.ArrayList;

import java.util.Iterator;

public class FormaComposta extends Forma {

    ArrayList<Forma> formas = new ArrayList<Forma>();

    @Override
    public float accept(Visitor visitor) {
        Iterator<Forma> it = this.formas.iterator();
        float areaTotal = 0;
        while(it.hasNext()) {
            areaTotal += it.next().accept(visitor);
        }
        return areaTotal;
    }

    public void addForma(Forma forma) {
        this.formas.add(forma);
    }


    
}
