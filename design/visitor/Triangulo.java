package visitor;

public class Triangulo extends Forma {

    public int base;
    public int altura;

    public Triangulo(int base, int altura) {
        this.altura = altura;
        this.base = base;
    }
    @Override
    public float accept(Visitor visitor) {

        return visitor.visite(this);
    }
    
}
