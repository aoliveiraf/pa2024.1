package visitor;

public class Visitor {
    public float visite(Triangulo visita) {
        return (visita.altura * visita.base)/2; 
    }
    public float visite(Retangulo visita) {
        return visita.lado * visita.altura;
    }
    public float visite(Trapezio visita) {
        return ((visita.baseMaior + visita.baseMenor)*visita.altura)/2;
    }

}
