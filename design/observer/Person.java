package observer;

import java.util.ArrayList;
import java.util.List;


public class Person implements Observer, Subject {
    private String name;

    private List<Observer> listObserver = new ArrayList<>();

    private List<String> listPost = new ArrayList<>();

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public void update(List<String> history) {
        System.out.println("fui notificado: " + this.name);
        System.out.println(history);
    }

    public void addPost(String post) {

        this.listPost.add(post);

        this.notify(listPost);
    }
    @Override
    public void addObserver(Observer observer) {
        
        this.listObserver.add(observer);
    }

    @Override
    public void notify(List<String> listPost) {
        
        for(int i=0;i< this.listObserver.size();i++) {

            Observer ob = this.listObserver.get(i);

            ob.update(listPost);
        }

    }
    
}
