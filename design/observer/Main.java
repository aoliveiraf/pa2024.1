package observer;

public class Main {
    

    public static void main(String[] args) {

        Person p_obs_1 = new Person("p1");
        Person p_obs_2 = new Person("p2");

        Person p_sub = new Person("p3");
        
        Bot b = new Bot("Bot 1");

        p_sub.addObserver(p_obs_1);
        p_sub.addObserver(p_obs_2);
        p_sub.addObserver(b);

        p_sub.addPost("Estou de volta.");      
        p_sub.addPost("Estou de volta 2.");      
        
    
    }

}
