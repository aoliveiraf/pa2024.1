package observer;

import java.util.List;

public class Bot implements Observer{

    private String name;
    public Bot(String name) {

        this.name = name;
    }
    @Override
    public void update(List<String> history) {
        
        System.out.println("Sou o bot " + this.name +  " e fui notificado");
        System.out.println(history);
        
    }
    
}
