package observer;

import java.util.List;

public interface Subject {
    
    public void addObserver(Observer observer);
    public void notify(List<String> listPost); 
}
